﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ConsoleApp18
{
    public partial class Form1 : Form
    {
        public void Write(string s)
        {
            this.richTextBox1.Text += s + '\n';
        }

        public Form1 partner = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            partner.Write( this.textBox1.Text);
        }
    }
}
