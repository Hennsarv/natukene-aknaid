﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ConsoleApp18
{
    class Program
    {
        static void Main(string[] args)
        {
            Form1 f1 = new Form1();
            Form1 f2 = new Form1();
            f1.partner = f2;
            f2.partner = f1;

            Thread t1 = new Thread(() => f1.ShowDialog());
            Thread t2 = new Thread(() => f2.ShowDialog());

            t1.Start();
            t2.Start();
        }
    }
}
