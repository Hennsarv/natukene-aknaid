﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp2
{
    public partial class Form1 : Form
    {
        Form1 partner;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            partner = new Form1();
            partner.partner = this ;
            partner.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (partner != null)
            {
                partner.Visible = true;
                partner.richTextBox1.Text += this.textBox1.Text + '\n';
                this.Visible = false;
            }
            else
                richTextBox1.Text += this.textBox1.Text + '\n';
        }
    }
}
